

// --- 1 ---
//  DOM – представляє весь контент сторінки як об’єкти, які можуть бути змінені.
//  Об’єкт document – це головна “точка входу” до сторінки. Там можна змінити або створити що-небудь на сторінці, використовуючи цей об’єкт

// --- 2 ---
// innerText повертає весь текст, що міститься в елементі, і всі його дочірні елементи.
// innerHtml повертає весь текст, включаючи теги html, який міститься в елементі.


// --- 3 ---
// Об'єкт Element можна отримати за допомогою методів:
// document.createElement()
// document.getElementById()
// document.getElementsByName()
// document.getElementsByTagName() 
// document.getElementsByClassName()
// document.querySelector() найбільш універсальний і гнучкий, оскільки можна використовувати будь-який CSS-селектор.
// document.querySelectorAll()



//+ --- 1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000


const findParagraph = document.querySelectorAll("p")
findParagraph.forEach(element => {
    element.style.backgroundColor = "#ff0000"
});



// + --- 2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionalList = document.getElementById("optionsList")
console.log(optionalList);

console.log(optionalList.parentNode);

if (optionalList.hasChildNodes()) {
    let childrens = optionalList.children;
    for (let i = 0; i < childrens.length; ++i) {
        console.log(`Node ${i + 1} - name: ${optionalList.childNodes[i].nodeName}, type: ${
            optionalList.childNodes[i].nodeType}`);
      }
    }

//+---3 Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph

const newParagraph = document.querySelector("#testParagraph")
newParagraph.innerHTML ="This is a paragraph"
console.log(newParagraph);

//+ --- 4 Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const mainHeader = document.querySelector(".main-header")


for (let index = 0; index < mainHeader.children.length; index++) {
    console.log(mainHeader.children[index]);
    mainHeader.children[index].className = "nav-item"
}

// + --- 5 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitle = document.querySelectorAll(".section-title")
console.log(sectionTitle);


for (let elem of sectionTitle) {
    elem.classList.remove("section-title");
}

